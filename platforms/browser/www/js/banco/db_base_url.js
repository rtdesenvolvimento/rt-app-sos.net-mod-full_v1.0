var db_base_url = {

    nm_table: function () {
        return "base_url";
    },

    colunas_create: function () {
        return "id integer primary key, base_url text";
    },

    colunas_save: function () {
        return   "base_url";
    },

    create: function (banco) {
        banco.transaction(function(tx) {
            tx.executeSql('CREATE TABLE IF NOT EXISTS '+db_base_url.nm_table()+' ('+db_base_url.colunas_create()+')');
        });
    },

    drop: function (banco) {
        banco.transaction(function(tx) {
            tx.executeSql('DROP TABLE '+db_base_url.nm_table());
        });
    },

    deleteAll: function (banco) {
        banco.transaction(function(tx) {
            tx.executeSql('DELETE FROM '+db_base_url.nm_table());
        });
    },

    save: function (banco, base_url) {

        db_base_url.deleteAll(banco);

        banco.transaction(function(tx) {
            dados = [base_url];
            tx.executeSql("INSERT INTO "+db_base_url.nm_table()+" ("+db_base_url.colunas_save()+") VALUES (?)", dados);
        });
    },

    buscar: function (banco) {
        banco.transaction(function(tx) {
            // Faz uma busca na tabela
            tx.executeSql("SELECT * FROM "+db_base_url.nm_table()+";", [], function(tx, res) {

                if (res.rows.length > 0) {
                    for (var i = 0; i < res.rows.length; i++) {
                        var base_url_consulta = res.rows.item(i).base_url
                        var base_url_digitada = prompt("Informe o IP do servidor", base_url_consulta);
                        var nova_base = [base_url_digitada];
                        db_base_url.save(banco.getBancoDados(), nova_base);
                    }
                } else {
                    var base_url_digitada = prompt("Informe o IP do servidor", base_url_consulta);
                    var nova_base = [base_url_digitada];
                    db_base_url.save(banco.getBancoDados(), nova_base);
                }
            });
        });
    },

    buscarBaseURL: function (banco) {
        banco.transaction(function(tx) {
            // Faz uma busca na tabela
            tx.executeSql("SELECT * FROM "+db_base_url.nm_table()+";", [], function(tx, res) {
                for (var i = 0;i<res.rows.length;i++){
                    base_url = res.rows.item(i).base_url
                }

                if (base_url === '') {
                    dados = ['http://localhost/rt-php-virtualfood-1.0/'];
                    tx.executeSql("INSERT INTO "+db_base_url.nm_table()+" ("+db_base_url.colunas_save()+") VALUES (?)", dados);
                }
            });
        });
    }

};