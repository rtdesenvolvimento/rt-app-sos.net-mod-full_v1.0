
var db_pedido_item = {

    nm_table: function () {
        return "pedido_item";
    },

    colunas_create: function () {
        return " id integer primary key, " +
            "   pedido_id integer," +
            "   pedido_mobile_id integer," +
            "   produto_id integer," +
            "   nome_produto text," +
            "   quantidade integer, " +
            "   valor integer, " +
            "   observacao text ";
    },

    colunas_save: function () {
        return   "pedido_id, pedido_mobile_id, produto_id, nome_produto, quantidade, valor, observacao";
    },

    create: function (banco) {
        banco.transaction(function(tx) {
            tx.executeSql('CREATE TABLE IF NOT EXISTS '+db_pedido_item.nm_table()+' ('+db_pedido_item.colunas_create()+')');
        });
    },

    drop: function (banco) {
        banco.transaction(function(tx) {
            tx.executeSql('DROP TABLE '+db_pedido_item.nm_table());
        });
    },

    deleteById: function (banco, id) {
        banco.transaction(function(tx) {
            tx.executeSql('DELETE FROM '+db_pedido_item.nm_table()+" WHERE pedido_id ="+id);
        });
    },

    deleteAll: function (banco) {
        banco.transaction(function(tx) {
            tx.executeSql('DELETE FROM '+db_pedido_item.nm_table());
        });
    },

    save: function (banco, pedido_item_save) {
        banco.transaction(function(tx) {
            tx.executeSql("INSERT INTO "+db_pedido_item.nm_table()+" ("+db_pedido_item.colunas_save()+") VALUES (?, ? , ?, ?, ? , ? , ? )", pedido_item_save,
                function () {
                    alert('callback');
                }, function () {
                    alert('errorCallback');
                });
        });
    },

    busca_byId: function (banco, id) {
        banco.transaction(function(tx) {
            // Faz uma busca na tabela
            tx.executeSql("SELECT * FROM "+db_pedidos.nm_table()+" WHERE pedido_id = "+id+";", [], function(tx, res) {
                for (var i = 0;i<res.rows.length;i++){
                    alert("Linha "+i+": "+res.rows.item(i).titulo);
                }
            });
        });
    },

    sincronizar: function (banco, base_url) {
        jQuery.ajax({
            url		: base_url + 'produtos/get_produtos_json/' ,
            type	: 'POST',
            cache	: false,
            async 	: false
        }).done(function(produtos){
            var produtos_json = JSON.parse(produtos);
            produtos_all = produtos_json;
            db_produtos.save(banco, produtos_json);
        });

    }

};